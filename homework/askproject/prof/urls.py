from django.urls import path

from prof.views import AskView, MyQuestionsView, ProfileQuestionsView
from . import views
from django.conf.urls import url, include

urlpatterns = [
    # path('<int:prof_id>', name='profile'),
    path('<int:prof_id>/my-questions', MyQuestionsView.as_view(), name='questions'),
    path('<int:prof_id>/ask-user', AskView.as_view(), name='form'),
    path('<int:prof_id>/', ProfileQuestionsView.as_view(), name='prof'),
]
