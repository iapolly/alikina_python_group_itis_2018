from django.db.models import F
from django.http import Http404
from django.shortcuts import render
from django.views.generic import FormView, ListView
from django.views.generic.edit import FormMixin

from prof.forms import QuestionForm
from prof.models import Question


class AskView(FormView):
    template_name = 'prof/ask.html'
    form_class = QuestionForm
    success_url = '/ask/'

    def form_valid(self, form):
        question = form.save(commit=False)
        request = self.request
        question.recipient = request.user
        return super(AskView, self).form_valid(question)


class FormListView(FormMixin, ListView):
    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        self.form = self.get_form(form_class)
        self.object_list = self.get_queryset()
        allow_empty = self.get_allow_empty()
        if not allow_empty and len(self.object_list) == 0:
            raise Http404(_(u"Empty list and '%(class_name)s.allow_empty' is False.")
                          % {'class_name': self.__class__.__name__})

        context = self.get_context_data(object_list=self.object_list, form=self.form)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)


class MyQuestionsView(ListView):
    template_name = 'prof/my-questions.html'
    # form_class =
    context_object_name = 'questions_list'

    def get_queryset(self):
        return Question.objects.filter(recipient_id=self.kwargs['prof_id'], answered=False)


class ProfileQuestionsView(ListView):
    template_name = 'prof/profile.html'
    # form_class =
    context_object_name = 'questions_list'

    def get_queryset(self):
        return Question.objects.filter(recipient_id=self.kwargs['prof_id'], answered=True)
