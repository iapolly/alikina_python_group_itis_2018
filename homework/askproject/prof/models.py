from django.contrib.auth.models import User
from django.db import models
from django.utils.timezone import now


class Question(models.Model):
    text = models.CharField(max_length=300, verbose_name="Текст вопроса")
    recipient = models.ForeignKey(User, on_delete=models.CASCADE)
    answer = models.CharField(max_length=300, verbose_name="Ответ", null=True)
    answered = models.BooleanField(default=False)
    date_added = models.DateTimeField(default=now)
