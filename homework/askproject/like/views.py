from django.contrib.auth.models import User
from django.http import JsonResponse

from like.models import Like
from prof.models import Question


def like(request):
    like = Like.objects.create()
    like.question = Question.objects.get(id=request.GET.get('post_id'))
    like.user = User.objects.get(request.user)
    save = like.save()
    return JsonResponse({'save', save})
