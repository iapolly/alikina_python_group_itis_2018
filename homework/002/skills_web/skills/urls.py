from . import views
from django.conf.urls import url, include
from django.urls import path

urlpatterns = [
    path('worker/<int:prof_id>', views.worker_skills, name='worker_skills'),
    path('workers/', views.workers, name='workers')
]