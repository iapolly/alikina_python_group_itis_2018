from django.db import models


class Worker(models.Model):
    first_name = models.CharField(max_length=50)
    second_name = models.CharField(max_length=50)
    skills = models.ManyToManyField('skills.Skill')
    

category = (('s', 'soft'), ('b', 'backend'), ('f', 'frontend'), ('t', 'testing'), ('a', 'analytics'))


class Skill(models.Model):
    title = models.CharField(max_length=50, primary_key=True)
    category = models.CharField(max_length=1, choices=category)
