from django.contrib import admin

from .models import Skill, Worker

admin.site.register(Skill)
admin.site.register(Worker)
