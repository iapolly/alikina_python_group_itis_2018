from django.shortcuts import render
from .models import Worker


def worker_skills(request, prof_id):
    worker = Worker.objects.get(id=prof_id)
    all_skills = worker.skills.all()
    rec_skills = recommend_skills(worker)
    context = {'worker': worker, 'all_skills': all_skills, 'recommend_skills': rec_skills}
    return render(request, 'skills/worker_skills.html', context)


def workers(request):
    all_workers = Worker.objects.all()
    context = {'workers': all_workers}
    return render(request, 'skills/workers.html', context)


def recommend_skills(worker):
    worker_skills = worker.skills.all()
    all_workers = Worker.objects.exclude(id=worker.id)
    skills_set = set()
    for wor in all_workers:
        other_worker_skills = wor.skills.all()
        c = worker_skills.intersection(other_worker_skills)
        m = 2 * len(c) / (len(worker_skills) + len(other_worker_skills))
        if m > 0.5:
            skills = set(other_worker_skills.difference(worker_skills))
            skills_set = skills_set.union(skills)
    return skills_set
