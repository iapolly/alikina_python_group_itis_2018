from django.core import validators
from django.forms import ModelForm, Textarea, FileInput, forms
from walls.models import Post


class PostForm(ModelForm):
    img = forms.FileField(validators=[validators.validate_image_file_extension])

    class Meta:
        model = Post
        fields = ['text', 'img']
        widgets = {
            'text': Textarea(attrs={'class': 'form-control'})
        }
