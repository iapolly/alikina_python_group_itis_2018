from django.contrib import admin

from prof.models import ProfilePage

admin.site.register(ProfilePage)