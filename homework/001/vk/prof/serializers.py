from rest_framework import serializers
from .models import ProfilePage
from django.contrib.auth.models import User


class ProfilePageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProfilePage
        fields = ('last_name', 'first_name')
