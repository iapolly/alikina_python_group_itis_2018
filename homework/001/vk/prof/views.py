from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render
from django.views.generic import TemplateView, ListView
from rest_framework.viewsets import ModelViewSet

from prof.models import ProfilePage
from walls.models import Post
from .forms import PostForm
from .serializers import ProfilePageSerializer


def prof_page(request, prof_url):
    profile = ProfilePage.objects.get(url_address=prof_url)
    user = request.user
    if request.method == 'POST' and user.has_perm('walls.add_post'):
        form = PostForm(request.POST, request.FILES)
        print(form.errors)
        if form.is_valid():
            post = form.save(commit=False)
            post.location = profile
            post.author = ProfilePage.objects.get(user=request.user)
            post.save()
    friends = ProfilePage.objects.order_by('?').filter(subscribe_to__id__contains=profile.id)
    posts = Post.objects.filter(location=profile).order_by('-date_added')
    form = PostForm()
    context = {'profile': profile, 'friends': friends, 'posts': posts, 'form': form}
    return render(request, 'prof/current_prof.html', context)


class ProfilePageViewSet(ModelViewSet):
    queryset = ProfilePage.objects.all()
    serializer_class = ProfilePageSerializer

    def perform_create(self, serializer):
        print('lalal')


class ProfilePageView(TemplateView):
    template_name = 'prof/current_prof.html'

    def get_context_data(self, **kwargs):
        context = super(ProfilePageView, self).get_context_data(**kwargs)
        prof_url = context.get('prof_url')
        profile = ProfilePage.objects.get(url_address=prof_url)
        context['profile'] = profile
        context['friends'] = ProfilePage.objects.order_by('?').filter(subscribe_to__id__contains=profile.id)
        context['posts'] = Post.objects.filter(location=profile).order_by('-date_added')
        context['form'] = PostForm()
        return context


class MyFriendsView(ListView):
    template_name = "prof/my_friends.html"

    context_object_name = 'friends'

    def get_queryset(self):
        subscribe_to_me = ProfilePage.objects.filter(Q(subscribe_to__url_address__contains=self.kwargs['prof_url']))
        return ProfilePage.objects.filter(subscribe_to=subscribe_to_me)
