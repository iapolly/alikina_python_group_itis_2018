from django.db import models
from page.models import Page
from community.models import VerifiedMixin
from documents.models import DocumentsMixin
from django.contrib.auth.models import User


class ProfilePage(Page, VerifiedMixin, DocumentsMixin):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    middle_name = models.CharField(max_length=40)
    d_o_b = models.DateField()
    subscribe_to = models.ManyToManyField('prof.ProfilePage', blank=True, related_name="subscribers")

