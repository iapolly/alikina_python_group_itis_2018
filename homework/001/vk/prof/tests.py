from django.contrib.auth.models import User
from django.test import TestCase, Client


class LogInTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_login(self):
        login = self.client.login(username='iapolly', password='iapolly')
        response = self.client.get('/prof/iapolly/', login)
        self.assertTrue(response.context['user'])