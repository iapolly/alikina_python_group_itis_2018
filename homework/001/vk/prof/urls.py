from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from prof.views import ProfilePageView, MyFriendsView
from . import views
from django.urls import path

urlpatterns = [
    # path('<str:prof_url>', views.prof_page)
    path('<str:prof_url>', login_required(ProfilePageView.as_view()), name='my_profile'),
    path('<str:prof_url>/friends', login_required(MyFriendsView.as_view()), name='my_friends'),
]