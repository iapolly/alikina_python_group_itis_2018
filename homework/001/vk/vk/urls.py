"""vk URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from rest_framework import routers
from prof import views as prof_views
from walls import views as walls_views

router = routers.DefaultRouter()
router.register(r'posts', walls_views.PostViewSet)
router.register(r'profiles', prof_views.ProfilePageViewSet)

urlpatterns = [
    path('login/', include('login.urls')),
    path('rest/', include(router.urls)),
    path('walls/', include('walls.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
    url('messages/', include(('msg.urls', 'msg'), namespace='msg')),
    url('prof/', include(('prof.urls', 'prof'), namespace='prof'))
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
