from django.contrib.auth.models import User
from django.forms import modelform_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.urls import reverse

from prof.models import ProfilePage


def login(request):
    args = {}
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            auth_login(request, user)
            page = ProfilePage.objects.get(user=user)
            return HttpResponseRedirect(reverse('prof:my_profile', args=(page.url_address, )))

        else:
            args['login_error'] = "Пользователь не найден"
            return render(request, 'login/login.html', args)
    elif request.method == 'GET':
        user = auth.get_user(request)
        if user.is_authenticated:
            page = ProfilePage.objects.get(user=user)
            return HttpResponseRedirect(reverse('prof:my_profile', args=(page.url_address,)))
        return render(request, 'login/login.html', args)


def logout(request):
    if auth.get_user(request).is_authenticated:
        auth_logout(request)
    return redirect('login')

