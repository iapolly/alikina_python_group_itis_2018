from django.contrib import admin

from videos.models import Video, Album

admin.site.register(Video)
admin.site.register(Album)
