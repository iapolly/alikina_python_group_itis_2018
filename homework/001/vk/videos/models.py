from django.db import models


class Video(models.Model):
    title = models.CharField(max_length=50)
    file = models.FileField()
    owner = models.OneToOneField('prof.ProfilePage', on_delete=models.CASCADE)
    album = models.ManyToManyField('videos.Album')


class Album(models.Model):
    title = models.CharField(max_length=50)
    location = models.OneToOneField('prof.ProfilePage', on_delete=models.CASCADE)
