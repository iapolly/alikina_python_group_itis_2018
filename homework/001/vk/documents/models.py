from django.db import models


class Document(models.Model):
    file = models.FileField()
    title = models.CharField(max_length=40)
    uploaded_by = models.ForeignKey('prof.ProfilePage', on_delete=models.CASCADE)


class DocumentsMixin(models.Model):
    class Meta:
        abstract = True
    documents = models.ManyToManyField('documents.Document', blank=True, related_name='document_owner')
