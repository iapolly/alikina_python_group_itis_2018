from django.db import models


class Photo(models.Model):
    title = models.CharField(max_length=50)
    file = models.FileField()
    album = models.ForeignKey('photos.Album', on_delete=models.CASCADE)
    owner = models.ForeignKey('prof.ProfilePage', on_delete=models.CASCADE)


class Album(models.Model):
    title = models.CharField(max_length=50)
