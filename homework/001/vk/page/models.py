from django.db import models
from django.utils.timezone import now


class Page(models.Model):
    class Meta:
        abstract = True
    creation_date = models.DateTimeField(default=now)
    url_address = models.SlugField()
    status = models.CharField(max_length=250)
    page_picture = models.OneToOneField('photos.Photo', blank=True, on_delete=models.CASCADE, null=True)
    location = models.ForeignKey('page.Location', on_delete=models.CASCADE)


class Location(models.Model):
    country = models.CharField(max_length=40)
    city = models.CharField(max_length=40)
