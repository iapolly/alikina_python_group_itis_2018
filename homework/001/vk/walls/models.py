from django.db import models
from django.utils.timezone import now


class Post(models.Model):
    author = models.ForeignKey('prof.ProfilePage', on_delete=models.CASCADE, related_name='post_author', blank=True)
    text = models.CharField(max_length=300)
    location = models.ForeignKey('prof.ProfilePage', on_delete=models.CASCADE, related_name='post_location')
    img = models.ImageField(upload_to='user_media', null=True)
    date_added = models.DateTimeField(default=now)
    likes = models.ManyToManyField('prof.ProfilePage', related_name="likes+")
    likes_count = models.IntegerField(null=True)
