# Generated by Django 2.1.7 on 2019-04-15 08:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('walls', '0010_auto_20190415_1123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='img',
            field=models.ImageField(upload_to='user_media'),
        ),
    ]
