from django.contrib.auth.decorators import permission_required
from django.db.models import F
from django.forms import modelform_factory
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect

from prof.models import ProfilePage
from .models import Post
from rest_framework import viewsets
from .serializers import PostSerializer
from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method == "POST":
            if request.user.is_authenticated and view.kwargs['id'] == request.user.id:
                return True
            else:
                return False
        else:
            return True


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


def like_post(req):
    post_id = req.GET.get('post_id', None)
    user_id = req.user.id

    post = Post.objects.get(id=post_id)
    prof = ProfilePage.objects.get(user_id=user_id)
    post.likes.add(prof)
    post.update(likes_count=F('like_count')+1)

    likes_count = post.likes.count()
    return JsonResponse({'data': likes_count})


@permission_required('walls.IsOwnerOrReadOnly')
def edit_post(req, post_id):
    post = Post.objects.get(id=post_id)
    if req.method == 'GET':
        return render(req, 'walls/edit_post.html', {'post': post})
    else:
        post.text = req.POST.get('text')
        post.img = req.POST.get('img')
        post.save()
        return HttpResponseRedirect('/login')
