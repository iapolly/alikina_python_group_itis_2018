from django.urls import path

from walls.views import like_post, edit_post

urlpatterns = [
    path('like/', like_post),
    path('edit/post/<int:post_id>', edit_post, name='edit_post')
]