from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now
from vk.settings import CUT_MSG_TEXT_LENGTH


class Mes(models.Model):
    sender = models.ForeignKey(User, related_name='msg_send', on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name='msg_received', on_delete=models.CASCADE)
    read = models.BooleanField(default=False)
    text = models.TextField()
    date_added = models.DateTimeField(default=now)

    def cut(self):
        if CUT_MSG_TEXT_LENGTH >= len(self.text):
            return self.text[:CUT_MSG_TEXT_LENGTH]
        else:
            return self.text[:CUT_MSG_TEXT_LENGTH] + '...'