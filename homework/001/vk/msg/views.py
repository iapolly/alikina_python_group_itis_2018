from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Mes
from .forms import MesForm
from django.views.generic import DetailView, FormView


class AllMessagesView(DetailView):
    template_name = "msg/current_msg.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['all_mes'] = Mes.objects.filter(receiver_id=self.request.user.id).order_by('-date_added')
        return context


def messages(request):
    all_mes = Mes.objects.filter(receiver_id=request.user.id).order_by('-date_added')
    context = {'all_mes': all_mes}
    return render(request, 'msg/current_msg.html', context)


class CurrentMessage(FormView):
    template_name = 'msg/msgs.html'
    form_class = MesForm
    def form_valid(self, form):
        mes = form.save(commit=False)



def message(request, message_id):
    msg = Mes.objects.get(id=message_id)
    if request.method == 'POST':
        form = MesForm(request.POST)
        if form.is_valid():
            mes = form.save(commit=False)
            mes.sender = msg.receiver
            mes.receiver = msg.sender
            mes.save()
            return HttpResponseRedirect('/messages/im/')
    else:
        if not msg.read:
            msg.read = True
            msg.save()
        form = MesForm()

    context = {'msg': msg, 'form': form}
    return render(request, 'msg/msgs.html', context)
