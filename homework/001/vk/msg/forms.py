from django.core.validators import MaxLengthValidator
from django.forms import ModelForm
from .models import Mes


class MesForm(ModelForm):
    class Meta:
        model = Mes
        fields = ['text']

