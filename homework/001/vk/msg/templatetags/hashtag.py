import datetime
from django import template
register = template.Library()


@register.filter
def make_hash_tag(login):
    return '<a href="/prof/'+str(login)+'">'+str(login)+'</a>'


@register.simple_tag
def current_time(format_string):
    return datetime.datetime.now().strftime(format_string)


# register.filter("make_hash_tag", make_hash_tag)