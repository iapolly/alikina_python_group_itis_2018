from . import views
from django.conf.urls import url, include
from msg.views import AllMessagesView

urlpatterns = [
    # url(r'^im/$', views.messages, name='messages'),
    url(r'^im/$', AllMessagesView.as_view()),
    url(r'^im/(?P<message_id>\d+)/$', views.message, name='message')
]