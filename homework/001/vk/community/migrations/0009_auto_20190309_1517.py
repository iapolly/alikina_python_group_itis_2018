# Generated by Django 2.1.6 on 2019-03-09 12:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('community', '0008_auto_20190309_1508'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='page_picture',
            field=models.OneToOneField(blank=True, on_delete=django.db.models.deletion.CASCADE, to='photos.Photo'),
        ),
        migrations.AlterField(
            model_name='meeting',
            name='page_picture',
            field=models.OneToOneField(blank=True, on_delete=django.db.models.deletion.CASCADE, to='photos.Photo'),
        ),
        migrations.AlterField(
            model_name='publicpage',
            name='page_picture',
            field=models.OneToOneField(blank=True, on_delete=django.db.models.deletion.CASCADE, to='photos.Photo'),
        ),
    ]
