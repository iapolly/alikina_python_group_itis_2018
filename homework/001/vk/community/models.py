from django.db import models
from page.models import Page
# from phonenumber_field.modelfields import PhoneNumberField
# from documents.models import DocumentsMixin

statuses = (('O', 'Open'), ('C', 'Close'), ('P', 'Private'))


class EstDateMixin(models.Model):
    class Meta:
        abstract = True
    est_date = models.DateField()


class VerifiedMixin(models.Model):
    class Meta:
        abstract = True
    verified = models.BooleanField(default=False)


class Contact(models.Model):
    occupation = models.ForeignKey('page.Location', on_delete=models.CASCADE)
    # phone = PhoneNumberField(null=False, blank=False, unique=True)
    email = models.EmailField(max_length=70, blank=True, null=True, unique=True)
    name = models.CharField(max_length=50)
    profile_page = models.ForeignKey('prof.ProfilePage', on_delete=models.CASCADE)


class Community(Page):
    class Meta:
        abstract = True
    title = models.CharField(max_length=50)
    information = models.TextField()
    # discussions
    # address = models.TextField()
    # # banner =
    # admin = models.ForeignKey('prof.ProfilePage', on_delete=models.CASCADE)
    # contacts = models.ManyToManyField('community.Contact')


class Meeting(Community):
    date = models.DateField()
    probable = models.ManyToManyField('prof.ProfilePage', related_name='probable')
    improbable = models.ManyToManyField('prof.ProfilePage', related_name='improbable')


class PublicPage(Community, EstDateMixin, VerifiedMixin):
    meeting = models.ForeignKey('Meeting', on_delete=models.CASCADE)


class Group(Community, EstDateMixin):
    open_status = models.CharField(max_length=1, choices=statuses)
    # menu = WikiPage

